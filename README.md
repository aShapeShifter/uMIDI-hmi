# uMIDI Human-Machine-Interface
A gamepad-like interface for [uMIDI](https://github.com/thefork/umidi).

![The uMIDI-HMI board in action](uMIDI-HMI.jpeg)


## Features
* 8 LEDs
* 2 toggle switches
* 2 rotary encoders (with switches)

## Pin mapping
The interface uses two GPIO banks of the uMIDI board:

**Outputs (LEDs)**

| Pin | Output |
|-----|--------|
| 2   | LED8   |
| 3   | LED1   |
| 4   | LED7   |
| 5   | LED2   |
| 6   | LED6   |
| 7   | LED3   |
| 8   | LED5   |
| 9   | LED4   |

**Inputs (Encoders and Switches)**

| Pin |        Input        |
|-----|---------------------|
| 2   | Encoder2 Switch     |
| 3   | Encoder1 Switch     |
| 4   | Encoder2 Terminal B |
| 5   | Encoder1 Terminal A |
| 6   | Encoder2 Terminal A |
| 7   | Encoder1 Terminal B |
| 8   | Switch2             |
| 9   | Switch1             |
